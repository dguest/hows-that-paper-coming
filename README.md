Paper Progress Tracker
======================

Did you ever wonder what you've been doing with your life? Maybe just
with the last few years of it. And maybe more specifically with the
parts that you spent at work. And specifically the part where you're
supposed to write papers?

Neat, this is a script to figure that out. You'll have to run
`get-edits-and-plot.sh` from the directory that contains your INT and
paper. Then you'll have to go in and manually change a bunch of the EB
and circulation dates.

Have fun.
