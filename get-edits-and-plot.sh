#!/usr/bin/env bash

function getlines() {
    (
        cd $1
        git log --pretty="@%ad" --shortstat --date=short --reverse *.tex | grep -v \| |  tr "\n" " "  |  tr "@" "\n" | awk '{print $1" "$2" "$5" "$7}'
    )
}

getlines *-INT1/ > int.txt
getlines *-PAPER/ > paper.txt

./make_edit_plot.py int.txt paper.txt

