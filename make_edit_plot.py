#!/usr/bin/env python3


from matplotlib.dates import datestr2num
import matplotlib.transforms as transforms
from matplotlib.pyplot import subplots
import sys
import argparse

def get_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('internal')
    parser.add_argument('paper')
    return parser.parse_args()


def add_date(ax, datestr, title, color='orange', yval=0.1):
    trans = transforms.blended_transform_factory(
        ax.transData, ax.transAxes)
    date = datestr2num(datestr)
    ax.axvline(date, color=color)
    ax.text(
        date, yval, title, color=color,
        rotation=90, transform=trans,
        va='bottom', ha='right')

def get_dates_and_lines(infile):
    total_edits = 0
    dates = []
    cumsum = []
    for line in infile:
        split = line.strip().split()
        if len(split) < 2:
            continue

        date, files, *add_or_delete = line.strip().split()
        total_edits += sum(int(x) for x in add_or_delete)
        cumsum.append(total_edits)
        dates.append(datestr2num(date))

    return dates, cumsum

def add_lines(ax, filename, sty, label):
    with open(filename) as entries:
        dates, cumsum = get_dates_and_lines(entries)
        ax.plot_date(dates, cumsum, sty, label=label)

def run():
    args = get_args()
    fig, ax = subplots()
    add_lines(ax, args.internal, '-', 'Int Note')
    add_lines(ax, args.paper, '-g', 'Paper')
    ax.legend()
    fig.autofmt_xdate()
    ax.set_xlim(left=datestr2num('2019-09'))
    ax.set_ylabel('Lines added or deleted')
    eb_meetings = ['2019-11-22', '2020-01-23', '2020-02-27', '2020-09-03',
                   '2020-11-01']
    for num, meeting in enumerate(eb_meetings):
        add_date(ax, meeting, f'EB {num+1}', 'orange', yval=0.6)
    add_date(ax, '2020-07-24', 'Exot Pre-Approval', 'blue')
    add_date(ax, '2020-10-14', 'Group Circulation', 'red')
    add_date(ax, '2020-11-04', 'Exotics Approval', 'blue')
    add_date(ax, '2021-01-29', 'Atlas Circulation', 'red')

    fig.savefig('commits.pdf', bbox_inches='tight')

if __name__ == '__main__':
    run()
